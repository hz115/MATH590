\documentclass[11pt]{article}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{graphicx}
\usepackage[sc]{mathpazo,xcolor}
\definecolor{newblue}{rgb}{0.2,0.2,0.6}
\usepackage[margin=0.9in]{geometry}
\usepackage{fancyhdr}
\usepackage{algorithm2e}
\usepackage[colorlinks,pagebackref,allcolors=newblue]{hyperref}
\pagestyle{fancyplain}
\usepackage{enumitem}
\renewcommand{\headrulewidth}{0pt}
\setlist[enumerate]{%
topsep=0pt,itemsep=0.3pt,
}

\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt plus 1pt}
\setlength{\headheight}{13.6pt}

\makeatletter
\renewcommand*\env@matrix[1][*\c@MaxMatrixCols c]{%
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{#1}}
\makeatother

\newenvironment{lbmatrix}[1]
  {\left[\array{@{}*{#1}{c}@{}}}
  {\endarray\right]}

\newcommand\question[2]{\vspace{.25in}\hrule\textbf{{\large #1: #2}}\vspace{.5em}\hrule\vspace{.10in}}
\lhead{\textbf{{\large \NAME\ (\ID)}}}
\chead{\textbf{{\large Problem Set \HWNUM}}}
\rhead{{\large MATH 590, Fall 17}}

\DeclareMathOperator{\E}{\mathbb{E}}

\begin{document}
\newcommand\NAME{Fred Zhang}
\newcommand\ID{hz115}
\newcommand\HWNUM{1}
\vspace*{-10mm}
\question{1}{When Crossover Probability is Half}
No. By the definition, the receiver would see a random bit each transmission, regardless of what the bit sent was. 
\question{2}{Square Code}
\textbf{(a).} Let $u,v\in \mathcal{C}$. Every row and column of $u$ sums to $0$, so does every row and column of $v$. By the commutativity and associativity of $+$ in $\mathbb{F}_2$, every row and column of $u+v$ sums to $0$. Hence, $u+v\in \mathcal{C}$. It follows that $\mathcal{C}$ is a binary linear code.

\textbf{(b).} Consider the following four vectors in $\mathbb{F}_2^9$, written in $3\times 3$ matrix.
\begin{equation*}
      u_1= \begin{bmatrix}1 & 1 & 0 \\1 & 1 &0 \\ 0 &0 &0 \end{bmatrix}, 
      u_2= \begin{bmatrix}0 & 1 & 1 \\0 & 1 &1 \\ 0 &0 &0 \end{bmatrix}, 
      u_3= \begin{bmatrix}0 & 0 & 0 \\1 & 1 &0 \\ 1 &1 &0 \end{bmatrix},
      u_4= \begin{bmatrix}0 & 0 & 0 \\0 & 1 &1 \\ 0 &1 &1 \end{bmatrix}
\end{equation*}
Obviously, all of them are in $\mathcal{C}$. Observe that they are linearly independent, as a corner entry $1$ is unique to one $u_i$. These four vectors form a basis for $\mathcal{C}$, and thus $\dim(\mathcal{C})=4$.

The generator and parity check matrix are given by 
\begin{equation*}
    G=\begin{bmatrix}[cccc|ccccc]
        1&0&0&0&1&1&1&0&0\\
        0&1&0&0&0&0&1&0&1\\
        0&0&1&0&1&1&0&0&1\\
        0&0&0&1&0&1&0&1&1
    \end{bmatrix},
    H=\begin{lbmatrix}{4}
        1&0&1&0\\
        1&0&1&1\\
        1&1&0&0\\
        0&0&0&1\\
        0&1&1&1\\
        \hline
        1&0&0&0\\
        0&1&0&0\\
        0&0&1&0\\
        0&0&0&1
    \end{lbmatrix}.
\end{equation*}

\textbf{(c)}. $A_\mathcal{C}(z) = 11z^4 + 5z^6$.

\question{3}{Coset Code}
We use the repetition code $R_3$ as a coset code, where the message length is $2$. In this case, there are $4$ cosets of $R_3$, each containing $2$ vectors, since $k=1$. They are given by
\[
\{111,000\}, \{101,010\}, \{110,001\}, \{011,100\}. 
\]
We associate each $2$-bit value with a coset.
\[
00 \longleftrightarrow \{111,000\}, 01 \longleftrightarrow \{101,010\}, 10\longleftrightarrow \{110,001\}, 11 \longleftrightarrow \{011,100\}. 
\]
For the first encoding, map $00$ to $000$, $01$ to $010$, $10$ to $001$ and $11$ to $100$. Note that each codeword is in a unique coset. Next observe that for the update, any codeword we have now can be changed into a vector in any other coset by writing $1$'s. 

\newpage
\question{4}{Dimension of Binary Linear Code}
Consider the generator matrix $G$ of $\mathcal{C}$ in the systematic form. Let $G=[I_k\mid B]$. Then $H=[B^\intercal\mid I_{N-k}]$ is its parity check matrix, \textit{i.e.}, the generator matrix of $\mathcal{C}^\perp$. Hence, $\mathcal{C}^\perp$ has dimension $N-k$. It follows that $\dim(\mathcal{C}) + \dim(\mathcal{C}^\perp)  = N$. 

\question{5}{Hamming code}
\textbf{(a).}  In both codes, a block error occurs when at least two bits get flipped over the BSC. The block error probability of the $[7,4,3]$ Hamming code is thus
\begin{equation}
    P_{(7,4)} = \sum_{c=2}^7 \binom{7}{c} p^c (1-p)^{7-c}.
\end{equation}
However, in the $[15, 11,3]$ Hamming code, the block error probability is
\begin{equation}
    P_{(15,11)} = \sum_{c=2}^{15} \binom{15}{c} p^c (1-p)^{15-c}.
\end{equation}
To compare to the two quantities, consider the probabilities that there is no block error;
\begin{align*}
    1-P_{(7,4)} &= (1-p)^7 + 7(1-p)^6 p = (1-p)^6 (1+6p)\\
    1-P_{(15,11)} &= (1-p)^{15} + 15(1-p)^{14} p = (1-p)^{14} (1+14p).
\end{align*}
Now since 
\[
    \frac{1-P_{(7,4)}}{1-P_{(15,11)}} = \frac{1+6p}{(1-p)^8(1+14p)},
\]
the shorter code would have lower block error probability, if the RHS is greater than $1$, or rather, $(1+6p)/(1+14p)>(1-p)^8$ for any $p$. 

This is indeed the case as $(1+6p)/(1+14p)=(1-p)^8$ when $p=0$, both LHS and RHS are monotone decreasing, and the derivative of $(1-p)^8$ is less for $p>0$.

\textbf{(b).} The rate of the longer code is less than the shorter code. It has less redundancy.

\question{6}{Hat Game}
\textbf{(a).} Consider the vector representation $v$ of the true hat colors; each entry is the correct hat color of the player (and there is no $*$). Since Hamming code is perfect and of distance $3$, any vector is of distance at most $1$ to a codeword. There are two cases:
\begin{enumerate}[label=(\roman*)]
    \item  The vector $v$ is exactly a codeword. Then every player's original vector is simply missing one entry. Therefore, they can all form this codeword $v$ and fill in their true hat color. Hence, everyone will guess; however, they will all be wrong, since the answer is the flip of the filled bit.
    \item The vector $v$ is of distance $1$ to its closest codeword $w$. Suppose $v$ and $w$ differs in the $j$th entry. Then player $j$ would form this codeword $w$, filling in the opposite of her hat color.  Observe that her answer would be correct because the answer is the flip  of the bit she filled in.
\end{enumerate}

\textbf{(b).} The true vector representation $v$ is a random vector in $\mathbb{F}_2^{N}$. The probability it is not a codeword is
\begin{equation}\label{eqn:noncode}
    \Pr[v \text{ not a codeword}] = 1-\frac{1}{2^m},
\end{equation}
since Hamming code is perfect and of distance $3$, and for every codeword, there are $2^m-1$ non-codewords of distance $1$ from it.

We argued that only in this case can any player possibly guess correctly. Now the probability that player $j$ guesses equals the probability that $v$ differs from its closest codeword in the $j$th entry. Because of the symmetry of the $N=2^m-1$ players, 
\[
    \Pr[\text{Player $j$ guesses}] = \frac{ \Pr[v \text{ not a codeword}] }{2^m-1} = \frac{1}{2^m}.
\]
As we saw earlier, player $j$ is certainly correct in this case.

\textbf{(c).} This happens when $v$ is indeed a codeword. The same argument in~(\ref{eqn:noncode}) applies, and we have $$\Pr[\text{Player $j$ guesses wrong}] = \frac{1}{2^m}.$$

\question{7}{Eavesdropping}
\textbf{(a).} Suppose that Alice and Bob uses the standard parity-check matrix $H$ for Hamming $[7,4,3]$ code, and Eve knows it, where
\[
H= \begin{pmatrix}
    1 & 0 & 1 &0 &1&0&1\\
    0&1&1&0&0&1&1\\
    0&0&0&1&1&1&1\\
\end{pmatrix}.
\]
Eve would know the $j$th bit of the syndrome for sure if fixing at most the $3$ bits that she eavesdropped, the potential syndromes $Hx$ for all possible $x$ have the same bit in the $j$th entry. Therefore, she would gain this bit if she knew all bits at positions of the ones in the $j$th row of $H$. Now note that every row  of $H$ has exactly $4$ ones. Then every row has at least one parity-check that is ``uncovered'' by Eve's known bits. Filling in $0$ or $1$ yields syndromes that differ on the $j$th entry. 

\textbf{(b).} Notice that the last four bits of the last row of $H$ are all ones. Eve can simply eavesdrop the last four bits. In this way, she could know that last bit of the syndrome. 

\textbf{(c).} Six.
\end{document}

