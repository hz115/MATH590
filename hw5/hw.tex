\documentclass[11pt]{article}
\usepackage{amsmath,amssymb,amsthm,xcolor}
\definecolor{newblue}{rgb}{0.2,0.2,0.6}
\usepackage[colorlinks,pagebackref,allcolors=newblue]{hyperref}
\usepackage{graphicx}
\usepackage[sc]{mathpazo}
\usepackage[margin=.9in]{geometry}
\usepackage{fancyhdr}
\usepackage{algorithm2e}

\pagestyle{fancyplain}
\renewcommand{\headrulewidth}{0pt}

\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt plus 1pt}
\setlength{\headheight}{13.6pt}

\renewcommand\textbullet{\ensuremath{\bullet}}
\newcommand\question[2]{\vspace{.25in}\hrule\textbf{{\large #1: #2}}\vspace{.5em}\hrule\vspace{.10in}}
\lhead{\textbf{{\large \NAME\ (\ID)}}}
\chead{\textbf{{\large Problem Set \HWNUM}}}
\rhead{{\large MATH 590, Fall 17}}

\DeclareMathOperator{\E}{\mathbb{E}}

\begin{document}
\newcommand\NAME{Fred Zhang}
\newcommand\ID{hz115}
\newcommand\HWNUM{5}
\vspace*{-10mm}
\question{1}{Euclid's Algorithm}
Since $r_N = \text{gcd}(a,b)$, it suffices to show that $s_Na+t_Nb= r_N$. We prove this by induction on $N$. For base case, notice that $s_{-1}a+t_{-1}b= a = r_{-1}$. Suppose as the induction hypothesis that 
\[
s_i a + t_i b = r_i
\]
for all $i \leq N-1$. Using the definition of the Euclid's algorithm,
\begin{align*}
    r_N &= r_{N-2}-q_Nr_{N-1}\\
        &= \left(s_{N-2}a + t_{N-1}b\right) - q_N (s_{N-1}a + t_{N-1}b) && (\text{by induction hypothesis})\\
        &=(s_{N-2}-q_Ns_{N-1})\cdot a + (t_{N-1}-q_Nt_{N-1})\cdot b\\
        &= s_Na + t_Nb. && (\text{by the recurrence relation for } s_N, t_N)
\end{align*}

\question{2}{Determine GCD}
We first show by induction that 
\begin{itemize}
    \item when $N$ is odd, $F_N = 2F_{N-1} + 1$; and
    \item when $N$ is even, $F_N= 2F_{N-1}-1$. 
\end{itemize}
For base case, if is sufficient to note that $F_1 = 2F_0 + 1$ and $F_2 = 2F_1-1$. Suppose this is true up to $N-1$. Consider $F_N = F_{N-1} + 2F_{N-2}$. If $N$ is odd, $N-1$ is even. Hence, by induction hypothesis, $F_{N-1} = 2F_{N-2}-1$, so $F_N = 2F_{N-1}+1$. If $N$ is even, then $N-2$ is odd. Hence, $F_{N-1} = 2F_{N-2}+1$, so $F_N = 2F_{N-1}-1$. This completes the induction step. 

Therefore, when $N$ is odd, $\text{gcd}(F_N,F_{N-1}) = 1$, since $F_N = 2F_{N-1}+1$. When $N$ is even, $F_N = F_{N-1}+ 2F_{N-2}$, and $2F_{N-2} <F_{N-1}$. We use Euclid's algorithm. Since $N-1$ is odd, $F_{N-1} = 2F_{N-2}+1$. This implies that $\text{gcd}(F_N,F_{N-1}) = 1$.

\question{3}{Compute Bezout's coefficients}
\textbf{(a).} We apply the Euclid's GCD algorithm on the two polynomials.
\begin{align*}
    x^8 &= \left(x^2+1\right) \left(x^6+x^4+x^2+x+1\right) + \left(x^3+x+1\right)\\
    x^6+x^4+x^2+x+1 &=(x^3+1) \left(x^3+x+1\right) + x^2 \\
    \left(x^3+x+1\right) &=x (x^2) + (x+1)\\
    x^2  &= x(x+1)+1. 
\end{align*}
Finally, we obtain $\text{gcd}(a,b) = 1$ and
\[
s= x^2 + x^3 + x^4 + x^5, t= 1 + x + x^3 + x^6 + x^7.
\]
That is, 
\[
    \left(1 + x + x^3 + x^6 + x^7\right)  x^ 8 + \left(1 + x + x^3 + x^6 + x^7\right) (x^6+x^4+x^2+x+1) = 1.
\] 

\textbf{(b).} We apply the Euclid's algorithm.
\begin{align*}
    3+4i & = (2+3i) + (1+i) \\
    2+3i &= 2(1+i) + i\\
    i+1  &= i + 1.
\end{align*}
We obtain that $\text{gcd}(a,b) = 1$ and 
\[
    -\frac{i}{6}(3+4i) + \frac{1}{6}(2+3i) = 1.
\]

\question{4}{Isomorphism}
Since $x^3+x+1$ is an irreducible polynomial over $\mathbb{F}_2$, the root $\alpha$ is a primitive element of $\mathbb{F}_2[\alpha]$. Similarly, $\beta$ is primitive as well. Hence, $\phi(\alpha)$ determines the isomorphism.   The three choices are $\phi(\alpha) = \beta$, $\phi(\alpha) = \beta^2$ and $\phi(\alpha)= \beta^4$.

\question{5}{Addition and Multiplication Table}
\textbf{(a).}
\begin{table}[htbp]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
$\cdot$   & $0$ & $1$       & $x$       & $x+1$     & $x^2$     & $x^2+1$   & $x^2+x$   & $x^2+x+1$ \\ \hline
$0$       &  $0$    &  $0$          & $0$           &    $0$        &     $0$       &   $0$         &     $0$       &    $0$        \\ \hline
$1$       &   $0$   & $1$       & $x$       & $x+1$     & $x^2$     & $x^2+1$   & $x^2+x$   & $x^2+x+1$ \\ \hline
$x$       & $0$ & $x$       & $x^2$     & $x^2+x$   & $x+1$     & $1$       & $x^2+x+1$ & $x^2+1$   \\ \hline
$x+1$     & $0$ & $x^2+x$   & $x^2+1$   & $x^2+1$   & $x^2+x+1$ & $x^2$     & $1$       & $x$       \\ \hline
$x^2$     &   $0$   & $x^2$     & $x+1$     & $x^2+x+1$ & $x^2+1$   & $x$       & $x^2+1$   & $1$       \\ \hline
$x^2+1$   &   $0$   & $x^2+1$   & $1$       & $x^2$     & $x$       & $x^2+x+1$ & $x+1$     & $x^2+x$   \\ \hline
$x^2+x$   &   $0$   & $x^2+x$   & $x^2+x+1$ & $1$       & $x^2+1$   & $x+1$     & $x$       & $x^2$     \\ \hline
$x^2+x+1$ & $0$ & $x^2+x+1$ & $x^2+1$   & $x$       & $1$       & $x^2+x$   & $x^2$     & $x+1$     \\ \hline
\end{tabular}
\caption{Multiplication table for $\mathbb{F}_8$ using using the primitive polynomial $p(x) = x^3 + x + 1$.}
\end{table}

\begin{table}[htbp]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
\hline
$+$       & $0$       & $1$       & $x$       & $x+1$     & $x^2$     & $x^2+1$   & $x^2+x$   & $x^2+x+1$ \\ \hline
$0$       & $0$       & $1$       & $x$       & $x+1$     & $x^2$     & $x^2+1$   & $x^2+x$   & $x^2+x+1$ \\ \hline
$1$       & $1$       & $0$       & $x+1$     & $x$       & $x^2+1$   & $x^2$     & $x^2+x+1$ & $x^2+x$   \\ \hline
$x$       & $x$       & $x+1$     & $0$       & $1$       & $x^2+x$   & $x^2+x+1$ & $x^2$     & $x^2+1$   \\ \hline
$x+1$     & $x+1$     & $x$       & $1$       & $0$       & $x^2+x+1$ & $x^2+x$   & $x^2+1$   & $x^2$     \\ \hline
$x^2$     & $x^2$     & $x^2+1$   & $x^2+x$   & $x^2+x+1$ & $0$       & $1$       & $x$       & $x+1$     \\ \hline
$x^2+1$   & $x^2+1$   & $x^2$     & $x^2+x+1$ & $x^2+x$   & $1$       & $0$       & $x+1$     & $x$       \\ \hline
$x^2+x$   & $x^2+x$   & $x^2+x+1$ & $x^2$     & $x^2+1$   & $x$       & $x+1$     & $0$       & $1$       \\ \hline
$x^2+x+1$ & $x^2+x+1$ & $x^2+x$   & $x^2+1$   & $x^2$     & $x+1$     & $x$       & $1$       & $0$       \\ \hline
\end{tabular}
\caption{Addition table for $\mathbb{F}_8$ using using the primitive polynomial $p(x) = x^3 + x + 1$.}
\end{table}
\newpage

\textbf{(b).}
\begin{table}[htbp]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|}
\hline
$\cdot$ & $0$ & $1$    & $2$    & $x$    & $x+1$  & $x+2$  & $2x$   & $2x+1$ & $2x+2$ \\ \hline
$0$     & $0$ & $0$    & $0$    & $0$    & $0$    & $0$    & $0$    & $0$    & $0$    \\ \hline
$1$     & $0$ & $1$    & $2$    & $x$    & $x+1$  & $x+2$  & $2x$   & $2x+1$ & $2x+2$ \\ \hline
$2$     & $0$ & $2$    & $1$    & $2x$   & $2x+2$ & $2x+1$ & $x$    & $x+2$  & $x+1$  \\ \hline
$x$     & $0$ & $x$    & $2x$   & $2x+1$ & $1$    & $x+1$  & $x+2$  & $2x+2$ & $2$    \\ \hline
$x+1$   & $0$ & $x+1$  & $2x+2$ & $1$    & $x+2$  & $2x$   & $2$    & $x$    & $2x+1$ \\ \hline
$x+2$   & $0$ & $x+2$  & $2x+1$ & $x+1$  & $2x$   & $2$    & $2x+2$ & $1$    & $x$    \\ \hline
$2x$    & $0$ & $2x$   & $x$    & $x+2$  & $2$    & $2x+2$ & $2x+1$ & $x+1$  & $1$    \\ \hline
$2x+1$  & $0$ & $2x+1$ & $x+2$  & $2x+2$ & $x$    & $1$    & $x+1$  & $2$    & $2x$   \\ \hline
$2x+2$  & $0$ & $2x+2$ & $x+1$  & $x^2$  & $2x+1$ & $x$    & $1$    & $2x$   & $x+2$  \\ \hline
\end{tabular}
\caption{Multiplication table for $\mathbb{F}_9$ using  primitive polynomial $p(x) = x^2 + x + 2$.}
\end{table}


\begin{table}[htbp]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|}
\hline
$+$    & $0$    & $1$    & $2$    & $x$    & $x+1$  & $x+2$  & $2x$   & $2x+1$ & $2x+2$ \\ \hline
$0$    & $0$    & $1$    & $2$    & $x$    & $x+1$  & $x+2$  & $2x$   & $2x+1$ & $2x+2$ \\ \hline
$1$    & $1$    & $2$    & $0$    & $x+1$  & $x+2$  & $x$    & $2x+1$ & $2x+2$ & $2x$   \\ \hline
$2$    & $2$    & $0$    & $1$    & $x+2$  & $x$    & $x+1$  & $2x+2$ & $2x$   & $2x+1$ \\ \hline
$x$    & $x$    & $x+1$  & $x+2$  & $2x$   & $2x+1$ & $2x+2$ & $0$    & $1$    & $2$    \\ \hline
$x+1$  & $x+1$  & $x+2$  & $x$    & $2x+1$ & $2x+2$ & $2x$   & $1$    & $2$    & $0$    \\ \hline
$x+2$  & $x+2$  & $x$    & $x+1$  & $2x+2$ & $2x$   & $2x+1$ & $2$    & $0$    & $1$    \\ \hline
$2x$   & $2x$   & $2x+1$ & $2x+2$ & $2x$   & $1$    & $2$    & $x$    & $x+1$  & $x+2$  \\ \hline
$2x+1$ & $2x+1$ & $2x+2$ & $2x$   & $1$    & $2$    & $0$    & $x+1$  & $x+2$  & $x$    \\ \hline
$2x+2$ & $2x+2$ & $2x$   & $2x+1$ & $2$    & $0$    & $1$    & $x+2$  & $x$    & $x+1$  \\ \hline
\end{tabular}
\caption{Addition table for $\mathbb{F}_9$ using  primitive polynomial $p(x) = x^2 + x + 2$.}
\end{table}

\question{6}{Primitive element}
\textbf{(a).} Notice that $x^6\, mod\, p(x) = x$. Therefore, $x$ does not generate all elements of the multiplicative group of $\mathbb{F}_{16}$. This means that $x$ is not primitive.

\textbf{(b).} We compute powers of $x+1$ mod $p(x)$.
\begin{align*}
    (x+1)^0 = 1, (x+1)^1= x+1, (x+1)^2 = x^2+1, (x+1)^3 =x^3+x^2+x+1\\
    (x+1)^4 = x^3+x^2+x, (x+1)^5= x^3+x^2+1, (x+1)^6 = x^3, (x+1)^7 =x^2+x+1\\
    (x+1)^8 = x^3+1, (x+1)^9= x^2, (x+1)^{10} = x^3+x^2, (x+1)^{11} =x^3+x+1\\
    (x+1)^{12} = x, (x+1)^{13}= x+x^2, (x+1)^{14} = x^3+x.
\end{align*}
This generates all elements of the multiplicative group of $\mathbb{F}_2/p(x)$. Hence, $x+1$ is primitive.

\textbf{(c).}  The minimal polynomial for $x+1$ is given by
\[
    f(\alpha) = (\alpha -(x+1))(\alpha -(x+1)^2)(\alpha -(x+1)^4)(\alpha -(x+1)^8) \,mod \, p(x) = 1+\alpha^3+\alpha^4.
\]

\newpage
\question{7}{Solve Equation}
Using the table and taking the difference of the last two equations, we get $x=\alpha^{12}, y=\alpha^{13}, z=\alpha^{10}$.

\question{8}{Minimal Polynomials}
\begin{table}[htbp]
\centering
\begin{tabular}{l|l}
\hline
Element                                        & Monomial Polynomial       \\ \hline
$1$                                            & $x+1$                     \\ 
$0$                                            & $x$                       \\ 
$\alpha,\alpha^2,\alpha^4,\alpha^8$            & $x^4+x+1$                 \\ 
$\alpha^3,\alpha^6,\alpha^9,\alpha^{12}$       & $x^4 + x^3 + x^2 + x + 1$ \\ 
$\alpha^5,\alpha^{10}$                         & $x^2+x+1$                 \\ 
$\alpha^7,\alpha^{11},\alpha^{13},\alpha^{14}$ & $x^4 + x^3 + 1$           \\ 
\end{tabular}
\caption{Minimal Polynomials}
\end{table}
The cyclotomic cosets modulo 15 are $\{0\},\{1,2,4,8\},\{3,6,9,12\},\{5,10\},\{7,11,13,14\}$.

\question{9}{Binary Polar Code}
\textbf{(a).} We use the Bayes' theorem and the fact that $X_i$ is uniformly random,
\begin{align*}
    z_i&= \Pr (X_i=1\mid Y_i=y_i)\\
       &= \frac{\Pr(Y_i=y_i\mid X_i=1)\Pr(x_i=1)}{\sum_{u\in\{0,1\}}\Pr(Y_i=y_i\mid x_i =u) \Pr (x_i=u)}\\
       &= \frac{W(y_i\mid 1)\cdot \frac{1}{2}}{\frac{1}{2}(W\left(y_i\mid 0\right) + W(y_i\mid 1))}\\
       &= \frac{W(y_i\mid 1)}{W(y_i \mid 0) + w(y_i \mid 1)}.
\end{align*}
Apply Bayes's rule again on the APP values.
\begin{align*}
    \Pr(U_1 = 1 \mid (Y_1,Y_2) = (y_1,y_2)) & = \frac{\Pr((Y_1,Y_2) = (y_1,y_2)\mid U_1 = 1) \Pr(U_1=1)}{\Pr((Y_1,Y_2) = (y_1,y_2))}\\
                                            & = \frac{\Pr (y_1y_2\mid U_1 = 1)\cdot \frac{1}{2}}{\sum_{u\in\{0,1\}} \Pr(y_1y_2 \mid U_1 =u)\cdot \frac{1}{2} }\\
                                            &= \frac{W(y_1\mid 1)W(y_2\mid 0) + W(y_1\mid 0)W(y_2\mid 0)}{\sum_{u\in \{0,1\}} \sum_{u'\in\{0,1\}}W(y_1\mid u\otimes u') W(y_2\mid u')}\\
                                            &= \frac{W(y_1\mid 1)W(y_2\mid 0) + W(y_1\mid 0)W(y_2\mid 0)}{\sum_{u\in \{0,1\}} W(y_1\mid u) W(y_2\mid 0) + W(y_1\mid u\otimes 1)W(y_2\mid 1)}\\
                                            &= \frac{W(y_1\mid 1)W(y_2\mid 0) + W(y_1\mid 0)W(y_2\mid 0)}{(W(y_1\mid 0) + W(y_1\mid 1)) (W(y_1\mid 0) +W(y_2\mid 1))}\\
                                            &= z_1(1-z_2) + z_2(1-z_1).
\end{align*}
\end{document}

