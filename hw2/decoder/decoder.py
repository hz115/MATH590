import numpy as np

def main():
    H = np.matrix([[1, 0, 1, 1, 1, 0, 0], [0, 1, 0, 1, 1, 1, 0], [0, 0, 1, 0, 1, 1, 1]])
    H2 = np.matrix('1 0 1 1 1 0 0;\
            0 1 0 1 1 1 0;\
            0 0 1 0 1 1 1;\
            1 0 0 1 0 1 1;\
            1 1 0 0 1 0 1;\
            1 1 1 0 0 1 0;\
            0 1 1 1 0 0 1')

    fraction = 0.
    ep = .1
    for i in range(100000):
        fraction += erasure_prob(H, ep) # one decoding test
    print(fraction / 100000.)

def erasure_prob(H, ep):
    received = BEC(rand_codeword(), ep)
    decoded = decode(H, received)
    return np.count_nonzero(decoded == -100) / 7.

def decode(H, r):
    stop = False
    num_check = H.shape[0]
    while (not stop):
        stop = True
        for check in range(num_check): # 3 / 7 checks
            idx = [] 
            bit1 = []
            for bit in range(7): # seven bits
                if r[0, bit] == -100 and H[check, bit] == 1:
                    idx.append(bit)
                if r[0, bit] != -100 and H[check, bit] == 1:
                    bit1.append(bit)
            if len(idx) >= 2 or len(idx) == 0:
                continue
            stop = False # Can correct one erasure
            s = 0 # Now corect the erasure
            for j in bit1:
                s += r[0, j]
            # print(idx[0])
            r[0, idx[0]] = s % 2
    return r

def BEC(codeword, ep): # Binary erasure channel with probablity ep
    for i in range(7):
        if np.random.random_sample() <= ep:
            codeword[0, i] = -100 
    return codeword

def rand_codeword(): # Return a random code word as row vector
    G = np.matrix([
        [1, 0, 1, 0, 0, 0, 1],
        [1, 1, 1, 0, 0, 1, 0],
        [0, 1, 1, 0, 1, 0, 0],
        [1, 1, 0, 1, 0, 0, 0]])
    msg = np.random.randint(2, size = 4)
    codeword = msg.dot(G)
    return np.mod(codeword, 2) 

if __name__ == '__main__':
    main()
