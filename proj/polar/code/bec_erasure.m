% Code for problem 1b

e = .3; n = 10;
E = e;
for i=1:n
    % Interleave updates to keep in polar decoding order
    E = reshape([1-(1-E).*(1-E); E.*E],1,[]);
end

p = sort(E);
x = [1:2^n]/2^n;
figure
title('Erasure Probabilities for p = .3 and n = 10')
xlabel('Index')
ylabel('Erasure Probabilities')
hold on
plot(x, p, 'x')
legend('n=10')
vline(.7, 'g', 'Capacity')
vline(.566, 'r', 'Rate')
hold on