% Load information set of code for BEC
becf = load('f.mat');
becf = becf.f;

% Load information set of code for BSC
load('bscf.mat');

% Each nonzero entry of becf and f is a information bit. 
% Take intersection and output size.
C = intersect(find(becf), find(f));
length(C)