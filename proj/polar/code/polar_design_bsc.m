%% Design of the code
n=10; d=.1; e=.05;
[f, A, bound, p] = polarcode_design_bsc(n,e,d);

%% Plot channel erasure probabilities
p = sort(p); x = [1:2^n]/2^n;
figure
title('Virtual Channel Error Probabilities for p = .05 and n = 10')
xlabel('Index')
ylabel('Erasure Probabilities')
hold on
plot(x, p, 'x')
legend('n=10')
vline(.7, 'g', 'Capacity')
vline(.575, 'r', 'Rate')
hold on

%% Test channels
pp = [0.001, 0.005, 0.01, 0.03, 0.043, 0.046, 0.05, 0.06, 0.1, 0.15];
error_rates = zeros(1, length(pp));

figure
title('Block error rate')
ylabel('Error rate')
xlabel('Channel erasure rate')
hold on

for i=1:length(pp)
    error_rates(i) = simulate_bsc(pp(i));
end

plot(pp, error_rates, '-x')
legend({'error rate', 'error rate'})
hold on

function errors = simulate_bsc(e)
M = 1000; N = 2^10;
f = importdata('bscf.mat'); k = sum(f==1/2);
biterr = zeros(1,M);
for i=1:M
    % Set frozen bits, add random data, and encode
    u = f;
    u(f==1/2) = rand(1,k)<0.5;
    % All zero experiment
    %u = zeros(1, 1024);
    
    % Encode
    x = polar_transform(u);
    
    % Transmit
    y = x;
    y = mod((y + (rand(1,N)<e)), 2);
    for j=1:N
        if y(j) == 1
            y(j) = 1-e;
        else
            y(j) = e;
        end
    end
    
    % Decode
    [uhat,~] = polar_decode(y,f);
    biterr(i) = mean(uhat~=u);
end
errors = mean(biterr>0);
end

%% Design polar code of length N=2^n for BEC(e) and
% target block error rate d
function [f, A, bound, SE] = polarcode_design_bsc(n,e,d)
% Generate virtual channel erasure probabilities

% Monte Carlo simulation for 10000 rounds
L=10000;

% All bits are information bits
f(1,1:2^n) = 1/2;
p = zeros(1, 2^n);
k=2^n;
N=2^n;
for i=1:L
    % Generate random strings
    u = f;
    u(f==1/2) = rand(1,k)<0.5;
    x = polar_transform(u);
    
    % Transmit
    y = x;
    y = mod((y + (rand(1,N)<e)), 2);
    for j=1:2^n
        if y(j) == 1
            y(j) = 1-e;
        else
            y(j) = e;
        end
    end
    
    % Decode
    [uhat, ~] = polar_decode2(y, u);
    for j=1:2^n
        if (uhat(j) ~= u(j))
            p(j) = 1 + p(j);
        end
    end
end
% Get probabilities
p = p / L;

% Sort into increasing order and compute cumulative sum
[SE,order] = sort(p);
CSE = cumsum(SE);

% Find good indices
I = sum(double(CSE<d));
A = order(1:I);
bound = sum(p(A));

% Set up prior probabilities
f = zeros(1,length(p));
f(order(1:I)) = 1/2;
end

%% Algorithm 3
function [u,x] = polar_decode2(y,uu)
% uu: true message
% y = bit APP from channel in output order

% Recurse down to length 1
N = length(y);
if (N==1)
    % If info bit, make hard decision based on observation
    x = (1-sign(1-2*y))/2;
    u = x;
    x = uu;
else
    % Compute soft mapping back one stage
    u1est = cnop(y(1:2:end),y(2:2:end));
    
    % R_N^T maps u1est to top polar code
    [uhat1,u1hardprev] = polar_decode2(u1est, uu(1:(N/2)));
    
    % Using u1est and x1hard, we can estimate u2
    u2est = vnop(cnop(u1hardprev,y(1:2:end)),y(2:2:end));
    
    % R_N^T maps u2est to bottom polar code
    [uhat2,u2hardprev] = polar_decode2(u2est, uu((N/2+1):end));
    
    % Tunnel u decisions back up. Compute and interleave x1,x2 hard decisions
    u = [uhat1 uhat2];
    x = reshape([cnop(u1hardprev,u2hardprev); u2hardprev],1,[]);
end
return
end

% Check-node operation in P1 domain
function z=cnop(w1,w2)
z = w1.*(1-w2) + w2.*(1-w1);
return
end

% Bit-node operation in P1 domain
function z=vnop(w1,w2)
z = w1.*w2 ./ (w1.*w2 + (1-w1).*(1-w2));
return
end