ep = [ 0.1, 0.15, 0.2, 0.23, 0.26, 0.3, 0.35, 0.5, 0.75, 1 ];
error_rates = zeros(1, length(ep));

figure
title('Block error rate')
ylabel('Error rate')
xlabel('Channel erasure rate')
hold on

for i=1:length(ep)
    error_rates(i) = simulate(ep(i));
end

plot(ep, error_rates, 'x')
legend('error rate')
hold on

function errors = simulate(e)
M = 1000; N = 2^10;
f = importdata('f.mat'); k = sum(f==1/2);
biterr = zeros(1,M);
for i=1:M
    % Set frozen bits, add random data, and encode
    u = f;
    u(f==1/2) = rand(1,k)<0.5;
    % All zero experiment
    %u = zeros(1, 1024);
    %x = polar_transform(u);
    
    % Transmit
    y = x;
    y(rand(1,N)<e)=1/2;
    
    % Decode
    [uhat,xhat] = polar_decode(y,f);
    biterr(i) = mean(uhat~=u);
end
errors = mean(biterr>0);
end