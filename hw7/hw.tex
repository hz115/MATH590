\documentclass[11pt]{article}
\usepackage{amsmath,amssymb,amsthm,xcolor}
\definecolor{newblue}{rgb}{0.2,0.2,0.6}
\usepackage[colorlinks,pagebackref,allcolors=newblue]{hyperref}
\usepackage{graphicx,microtype}
\usepackage[sc]{mathpazo}
\usepackage[margin=.9in]{geometry}
\usepackage{fancyhdr}
\usepackage{enumitem}
\usepackage{algorithm2e}

\pagestyle{fancyplain}
\renewcommand{\headrulewidth}{0pt}

\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt plus 1pt}
\setlength{\headheight}{13.6pt}

\newcommand\question[2]{\vspace{.25in}\hrule\textbf{{\large #1: #2}}\vspace{.5em}\hrule\vspace{.10in}}
\lhead{\textbf{{\large \NAME\ (\ID)}}}
\chead{\textbf{{\large Problem Set \HWNUM}}}
\rhead{{\large MATH 590, Fall 17}}

\DeclareMathOperator{\E}{\mathbb{E}}
\renewcommand\textbullet{\ensuremath{\bullet}}

\begin{document}
\newcommand\NAME{Fred Zhang}
\newcommand\ID{hz115}
\newcommand\HWNUM{7}
\vspace*{-10mm}
\question{1}{[[5, 1, 3]] Code}
For a code to be CSS code of dimension $k_1-k_2$, there exists two classical codes of dimensions $k_1,k_2$.  By Hamming bound, any code of length $n$, dimension $k$, and distance $3$ must satify
\[
k \leq n - \log_2 n.
\]
In particular, for $n=5$, this implies that $k<3$.

Thus, [[5,1,3]] code must be constructed from a $[5,2,1]$ code $C_1$ and a $[5,1,1]$ code $C_2$, and $C_1,C_2^\perp$ are of distance $3$. But notice that $C_2^\perp$ is a $[5,4,3]$ binary linear code, and this violates the Hamming bound. (Recall that the $[7,4,3]$ Hamming code is perfect.)

\question{2}{CSS Code}
\textbf{(a).} The generator matrix is given by
\[
G_S=\begin{pmatrix}
    0&G_1^\perp\\
    G_2&0
\end{pmatrix}_{(n-k_1+k_2)\times 2n}.
\]
Each row of the matrix is of the form $v=(0|b)$ or $v=(a|0)$. Take any two rows of the matrix $v=(a|b),v'=(a'|b')$. The symplectic inner products are given by 
\begin{align*}
    v\cdot v' = ab'+ba'\\
    v'\cdot v = a'b+b'a.
\end{align*}
Then one of the three cases must hold:
\begin{enumerate}
    \item $a = a' =0$,
    \item $b=b'=0$,
    \item $a =  b' = 0$ or $a'=b=0$.
\end{enumerate}
In any of the cases, the two symplectic inner products are both $0$, and thus the operators commute in $\mathbb{F}_{2}^{2n}$.

\textbf{(b).} Each row in $G_S$ is either of the form $(0|h)$ or of the form $(g|0)$, where $h$ is a parity-check of $C_1$ and $g$ is a row of the generator matrix $G_1$. For any $v\in S$, $v$ is a linear combination of the rows in $G_s$. It suffices to show that $D(a,b)$ fixes $|\psi_v\rangle$ for $(a|b)$ that is a row of $G_S$.
\begin{itemize}
    \item Let $w= (0|h)$ be a row in $G_S$. We apply $D(0,h)$ to $|\psi_v\rangle$ and obtain
        \begin{align*}
            D(0,h)|\psi_v\rangle &=D(0,h) \frac{1}{\sqrt{|C_2|}}\sum_{c\in C_2} |c+v\rangle\\
                                 &=  \frac{1}{\sqrt{|C_2|}}\sum_{c\in C_2} D(0,h) |c+v\rangle\\
                                 &=   \frac{1}{\sqrt{|C_2|}}\sum_{c\in C_2} (-1)^{(c+v)\cdot h} |c+v\rangle\\
                                 &=    \frac{1}{\sqrt{|C_2|}}\sum_{c\in C_2}  |c+v\rangle && \text{(since $h$ is a parity-check of $G_1$.)}\\
                                 &= |\psi_v\rangle.
        \end{align*}
    \item Let $w= (g|0)$ be a row in $G_S$, where $g\in C_2$, and we apply $D(g,0)$ to $|\psi_v\rangle$:
        \begin{align*}
            D(g,0)|\psi_v\rangle &=D(g,0) \frac{1}{\sqrt{|C_2|}}\sum_{c\in C_2} |c+v\rangle\\
                                 &=  \frac{1}{\sqrt{|C_2|}}\sum_{c\in C_2} D(g,0) |c+v\rangle\\
                                 &=   \frac{1}{\sqrt{|C_2|}}\sum_{c\in C_2}  |c+v + g\rangle\\
                                 &= |\psi_v\rangle.
        \end{align*}
        The last equality follows since the sum is taken over all $c\in C_2$ and $g\in C_2$.
\end{itemize}

\question{3}{Additive Code}
\textbf{(a).} Let $v= (a,b)$ and $v'=(a,b)$ be two elements in $\mathbb{F}_2^{2n}$. The (symplectic) inner product is 
\[
v\cdot v' = a\cdot b'+b\cdot a'.
\]
By definition, $\phi(v) = wa+\overline{w} b$ and $\phi(v') = wa'+\overline{w}b'$, so $\overline{\phi(v')} = \overline{w}a' + wb'$. Therefore,
\[
    \phi(v)\cdot \overline{\phi(v')} = w\overline{w}a\cdot a' + \overline{w}^2 a'\cdot b + w^2 a\cdot b' + w\overline{w} b\cdot b'.
\]
It follows from calculation that $Tr\left(\phi\left(v\right)\cdot \overline{\phi(v')}\right)  = a\cdot b' + b\cdot a'=v\cdot v'$. Thus, the mapping $\phi$ preserves inner products.

\textbf{(b).}  Let $a,b,c,d \in \mathbb{F}_2^{2n}$ such that $(a,b), (c,d)$ are in a additive code $C\subseteq \mathbb{F}_{2}^{2n}$.  Firt consider the bijection $\psi: \mathbb{F}_2^{2n} \rightarrow \mathbb{F}_4^n$, defined by $\psi (a,b) =wa+\overline{w}b$. It is isomorphic, since the mapping preserves inner products. Also, by the property of the Heisenberg-Weyl group, $D(a,b)D(c,d) = D(c,d)D(a,b)$ if and only if the symplectic inner product $\langle (a,b) , (c,d) \rangle_{sym} = 0$. As what saw in \textbf{(a)}, this is equivalent of $\langle u,v\rangle_{tr}= 0$, where $u=wc+\overline{w}d$ and $v= wa+\overline{w}b$. Now consider the the bijection $\psi: \mathbb{F}_4^n  \rightarrow  HW_{2^n}$ defined by $\psi((a,b)) = D(a,b)$. Since the image of $\psi$ must be a commutative subgroup of $HW_{2^n}$ for it to form a stabalizer code, the inverse image must also be commutative. But this is precisely what we showed in \textbf{2(a)}, when the inverse image is just the image of $\psi$, and thus the mapping $\phi\circ\psi$ is isomorphic.  Therefore, from any stabalizer code $S$, $\phi \circ \psi$ yields a isomorphic additive code $C$ over $\mathbb{F}_4$.

On the other hand, let $S$ be a stabalizer code. Then $D(a,b)$ and $D(c,d)$ commute for $D(a,b),D(c,d) \in S$. As we have shown, this implies that $\langle u,v\rangle_{sym} =0$ for all $u,v$. This gives a self-orthorgonal additive code.
\end{document}
