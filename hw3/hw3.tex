\documentclass[11pt]{article}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{graphicx}
\usepackage[sc]{mathpazo}
\usepackage[margin=.9in]{geometry}
\usepackage{fancyhdr}
\usepackage{algorithm2e}
\usepackage{enumitem}

\pagestyle{fancyplain}
\renewcommand{\headrulewidth}{0pt}

\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt plus 1pt}
\setlength{\headheight}{13.6pt}

\newcommand\question[2]{\vspace{.25in}\hrule\textbf{{\large #1: #2}}\vspace{.5em}\hrule\vspace{.10in}}
\lhead{\textbf{{\large \NAME\ (\ID)}}}
\chead{\textbf{{\large Problem Set \HWNUM}}}
\rhead{{\large MATH 590, Fall 17}}

\DeclareMathOperator{\E}{\mathbb{E}}

\begin{document}
\newcommand\NAME{Fred Zhang}
\newcommand\ID{hz115}
\newcommand\HWNUM{3}
\vspace*{-10mm}
\question{1}{$HW4$ as Kronecker product of $HW2$}
\[
\left[
\begin{array}{cc|cc}
+ &   &   &       \\
 & + &  &    \\
\hline
 &  & + &    \\
 & & & +
\end{array}
\right] =
\begin{pmatrix}
    + & \\
      &+
\end{pmatrix}\otimes
\begin{pmatrix}
    + & \\
      &+
\end{pmatrix} = D(0,0) \otimes D(0,0).
\]
\[
\left[
\begin{array}{cc|cc}
 & +  &   &       \\
 -&  &  &    \\
\hline
 &  &  & -   \\
 & & +& 
\end{array}
\right] =
-\begin{pmatrix}
     +& \\
      &-
\end{pmatrix}\otimes
\begin{pmatrix}
     &- \\
     + &
\end{pmatrix} = -D(0,1) \otimes D(1,1).
\]
\[
\left[
\begin{array}{cc|cc}
 &   & -  &       \\
 &  &  & -   \\
\hline
+ &  &  &   \\
 & +& & 
\end{array}
\right] =
\begin{pmatrix}
     & -\\
      +&
\end{pmatrix}\otimes
\begin{pmatrix}
     +& \\
      &+
\end{pmatrix} = D(1,1) \otimes D(0,0).
\]
\[
\left[
\begin{array}{cc|cc}
 &   &   & -      \\
 &  & + &    \\
\hline
 & - &  &   \\
 +& & & 
\end{array}
\right] =
\begin{pmatrix}
     & +\\
      +&
\end{pmatrix}\otimes
\begin{pmatrix}
     & -\\
     + &
\end{pmatrix} = D(1,0) \otimes D(1,1).
\]
\question{2}{Kronecker product of unit coordinate vector}
We prove the claim by induction. When $m=1$, the claim is self-evident, as $e_0=e_0$ and $e_1=e_1$. Let $v' = (v_{m-2},\ldots,v_0)$, and assume as an induction hypothesis that $e_{v'} = e_{v_{m-2}} \otimes \cdots \otimes e_{v_0}$. To complete the induction step, it suffices to show that $e_v = e_{v_{m-1}} \otimes e_{v'}$. There are two cases.
\begin{itemize}
    \item If $v_{m-1}=0$, then $$e_0 \otimes e_{v'} = \left(e_{v'} \ 0\cdots 0\right).$$
        This is exactly $e_{v}$ because, in this case, the position of the $1$ in $e_v$, counting from left end to right, is the same in $e_{v'}$
    \item If $v_{m-1}=1$, then 
        \[
        e_1 \otimes e_{v'} = \left( 0\cdots 0 \ e_{v'}\right).
        \]
        Again, this equals $e_v$ because, in this case, $v_{m-1}=1$ implies that the first $2^{m-1}$ entries of $e_v$ are zero and counting from the $(2^{m-1}+1)$st entry, the $1$ is at position $v'$.
\end{itemize}

\question{3}{Time shift group}
When $m=1$, observe from the table, $e_bX^{b'} = e_{b+b'}$. Hence, $e_vD(a,0)=e_{v+a}$. Let $v'=(v_{m-2},\ldots,v_0)$ and $a'=(a_{m-2},\ldots,a_0)$. Assume now for an induction that $e_{v'}D(a',0)=e_{v'+a'}$. We first expand $e_vD(a,0)$ as tensor product, using the definition of $D(a,0)$ and the fact that $e_v=e_{v_{m-1}}\otimes \cdots\otimes e_{v_{0}}$:
\begin{align*}
    e_vD(a,0) &= \left(e_{v_m-1}\otimes e_{v'}\right) \left(X^{a_{m-1}}\otimes D(a',0)\right)\\
              &= \left(e_{v_{m-1}}X^{a_{m-1}}\right)\otimes(e_{v'}D(a',0)) && (\text{using the identity } (A\otimes B )(C\otimes D) = (AC)\otimes (BD).)\\
              &= e_{v_{m-1}+a_{m-1}}\otimes e_{v'+a'}&&(\text{applying induction hypothesis.})\\
              &=e_{v+a}
\end{align*}

\question{4}{Frequency shift group}
When $m=1$, $v$ and $b$ are both a single bit. Then the claim simply follows from the given table of $e_vD(0,b)$ in this case. Let $v'=(v_{m-2},\ldots,v_0)$ and $a'=(a_{m-2},\ldots,a_0)$.  Assume for induction that $e_{v'}D(0,b') = (-1)^{b'v'^T} e_{v'}$. Again, express $e_vD(0,b)$ as a tensor product.
\begin{align*}
    e_vD(0,b) &= \left(e_{v_m-1}\otimes e_{v'}\right) \left(Z^{a_{m-1}}\otimes D(0,b')\right)\\
              &= \left(e_{v_{m-1}}Z^{a_{m-1}}\right)\otimes(e_{v'}D(0,b')) && (\text{using the identity } (A\otimes B )(C\otimes D) = (AC)\otimes (BD).)\\
              &= (-1)^{v_{m-1}a_{m-1}} e_{v_{m-1}}\otimes (-1)^{v'b'}e_{v'}&&(\text{applying induction hypothesis.})\\
              &=(-1)^{b'v'^T} e_{v'}
\end{align*}

\question{5}{Heisenberg-Weyl group}
\textbf{(a).}  To show that $HW_N$ is a group, we observe the following.
\begin{itemize}
    \item \textit{Closure}: The matrix can be expressed $D(a,b)$ as a tensor product as follows
        \begin{align*}
            D(a,b) &= D(a,0)D(0,b)\\
                   &=  \left(X^{a_{m-1}} \otimes \cdots \otimes X^{a_0}\right) \left(Z^{b_{m-1}} \otimes \cdots \otimes Z^{b_0}\right)\\
                   &= \left(X^{a_{m-1}} \cdot Z^{b_{m-1}}\right)\otimes \left(\left(X^{a_{m-2}} \otimes \cdots \otimes X^{a_0}\right)\left(Z^{b_{m-2}} \otimes \cdots \otimes Z^{b_0}\right)\right)\\
                   &=\left(X^{a_{m-1}} \cdot Z^{b_{m-1}}\right)\otimes\left(X^{a_{m-2}} \cdot Z^{b_{m-2}}\right) \otimes\left(\left(X^{a_{m-3}} \otimes \cdots \otimes X^{a_0}\right)\left(Z^{b_{m-3}} \otimes \cdots \otimes Z^{b_0}\right)\right)\\
                   &\quad \vdots \\ 
                   &=\bigotimes_{i=0}^{m-1} \left(X^{a_{i}} \cdot Z^{b_{i}}\right)
        \end{align*}
        The last line is obtained by applying the identity $(A\otimes B )(C\otimes D) = (AC)\otimes (BD)$ on all components. Hence, $D(a,b)$ can be defined as a tensor product of $X,Z,\pm XZ, I_2$. Now consider
        \begin{align*}
            D(a,b)D(c,d) &= \left(\bigotimes_{i=0}^{m-1} \left(X^{a_{i}} \cdot Z^{b_{i}}\right)\right)\cdot\left(\bigotimes_{i=0}^{m-1} \left(X^{c_{i}} \cdot Z^{d_{i}}\right)\right)\\
                         &=\bigotimes_{i=0}^{m-1} \left(X^{a_{i}} \cdot Z^{b_{i}}\cdot X^{c_{i}} \cdot Z^{d_{i}}\right)\\
                         &=\bigotimes_{i=0}^{m-1} (-1)^{b_i c_i}\cdot \left(X^{a_i+c_i} Z^{b_i+d_i}\right) \\
                         &=(-1)^{bc^T} D(a+c,b+d)\in HW_N.
        \end{align*}
        The succinct form of the second last equality can be obtained by enumerating the $16$ possibilities on the values of $a_i,b_i,c_i,d_i$. In general, we conclude that $(\delta D(a,b))(\delta' D(c,d)) = \delta\delta'(-1)^{bc} D(a+c,b+d)\in HW_N$, where $\delta,\delta\in \{\pm 1,\pm i\}$.
    \item  \textit{Identity}: Notice that $D(0,0)= I_N$. This serves as the identity of $HW_N$, as $D(a,b) \cdot D(0,0)= D(a,b)$.
    \item \textit{Associativity} follows from associativity of matrix multiplication.
    \item  \textit{Inverse}: First recall that $Z=Z^{-1}$ and $X=X^{-1}$. Let $a$ be a binary $m$-tuple, $a'  = \left(a_{m-2},\ldots, a_0\right)$ and $N=2^m$. Suppose as an induction hypothesis $D(a',0)D(a',0)=I_{N/2}$. We consider $D(a,0)D(a,0)$ and express it as a tensor product.
\begin{align*}
    D(a,0)D(a,0) &= \left(X^{a_{m-1}} \otimes \cdots \otimes X^{a_0}\right)\cdot \left(X^{a_{m-1}} \otimes \cdots \otimes X^{a_0}\right)\\
                 &= (X^{a_{m-1}} \cdot X^{a_{m-1}})\otimes \left(\left(X^{a_{m-2}} \otimes \cdots \otimes X^{a_0}\right)\left(X^{a_{m-2}} \otimes \cdots \otimes X^{a_0}\right)\right)\\
                 &= I_2 \otimes (D(a',0)D(a',0))\\
                 &= I_N
\end{align*}
The last line follows from induction hypothesis and the definition of Kronecker product. Hence, $D(a,0)^{-1} = D(a,0)$. Moreover, $D(0,b)^{-1}= D(0,b)$ via exactly the same argument. 

Thus, $D(a,b)D(a,b)^T =D(a,0)D(0,b) D(0,b)D(a,0) = I$, using the fact that $D(a,0)$ and $D(0,b)$ are symmetric.  Therefore, $\pm D(a,b)^{-1}=\pm D(a,b)^T$ and $\pm iD(a,b) = \mp iD(b,a)^T$.
\end{itemize}

Finally, as we have shown,
\[
    D(a',b')D(a,b)= (-1)^{a'b^T}D(a+a',b+b').
\]
By multiplying $(-1)^{a'b^T}$ on both sides,
\[
    D(a+a',b+b') =(-1)^{a'b^T} D(a',b')D(a,b)
\]
Now we have
\begin{align*}
    D(a,b)D(a',b') &= (-1)^{b'a^T}D(a+a',b+b')\\
                   &= (-1)^{b'a^T}\cdot(-1)^{a'b^T} D(a',b')D(a,b)\\
                   &=(-1)^{a'b^T+ b'a^T} D(a',b')D(a,b).
\end{align*}
\textbf{(b).} 
As we showed, $D(a,b)D(a,b)^T = I_N$. Hence, it suffices to show that 
\[
    D(a,b)^T D(a,b)^T = (-1)^{ab^T} I_N
\]
We focus on the left-hand side.
\begin{align*}
    D(a,b)^T D(a,b)^T &=  \left(\bigotimes_{i=0}^{m-1} \left(X^{a_{i}} \cdot Z^{b_{i}}\right)\right)^T\left(\bigotimes_{i=0}^{m-1} \left(X^{a_{i}} \cdot Z^{b_{i}}\right)\right)^T\\
                      &=   \left(\bigotimes_{i=0}^{m-1} \left(X^{a_{i}} \cdot Z^{b_{i}}\right)^T\right)\left(\bigotimes_{i=0}^{m-1} \left(X^{a_{i}} \cdot Z^{b_{i}}\right)^T\right) &&(\text{since }\left(A\otimes B\right)^T = A^T\otimes B^T) \\
                      &= \left(\bigotimes_{i=1}^{m-1} Z^{b_i}X^{a_i}\right)\left(\bigotimes_{i=1}^{m-1} Z^{b_i}X^{a_i}\right)\\
                      &= \bigotimes_{i=1}^{m-1} Z^{b_i}X^{a_i}Z^{b_i}X^{a_i}\\
                      &= \bigotimes_{i=1}^{m-1}(-1)^{a_ib_i} I_2 && (\text{by enumerating the values of } b_i, a_i)\\
                      &= (-1)^{ab^T}I_N
\end{align*}

\textbf{(c).} The identity has order $1$. Let us consider $D(a,b)^2$ and see what element are of order $2$.
\begin{align*}
    D(a,b)D(a,b)&= \bigotimes_{i=0}^{m-1} \left(X^{a_{i}} \cdot Z^{b_{i}}\cdot X^{a_{i}} \cdot Z^{b_{i}}\right)\\
                &=  \bigotimes_{i=0}^{m-1} (-1)^{a_i b_i} I_2\\
                &= (-1)^{ab^T} I_N
\end{align*}
If $ab^T=0$, then $\pm D(a,b)$ and $-i D(a,b)$ are of order $2$. For all those $D(a,b)$ that are not order $2$, $D(a,b)^4=I_N$ by the above calculation.

Hence, elements $\pm D(a,b)$ such that $a b^T = 1$ and $iD(a,b)$ (for any $a,b$) are of order $4$.

\question{6}{$E(a,b)$}
\textbf{(a).} To show that $E(a,b)$ is Hermitian, it suffices to prove that $D(a,b)$ is symmetric if $ab^T=0$ and antisymmetric if $ab^T=1$. Notice that when $a,b$ are both $1$-bit, $XZ$ is antisymmetric, while $X,Z,I$ are symmetric. Let $a'=(a_{m-2},\ldots,a_0)$ and $b' = (b_{m-2},\ldots,b_0)$. Assume as an induction hypothesis that $D(a',b')$ is symmetric if and only if $ab^T=0$. Clearly, $D(a,b)$ is symmetric when there is no position $i$ such that $X^{a_i}Z^{b_i}=XZ$ because it is a tensor product of symmetric matrices.  Without loss of generality, suppose that the first position where $X^{a_i}Z^{b_i}=XZ$ is $m-1$, the most significant bit. We start by expressing $D(a,b)$ as a tensor product.
\begin{align*}
    D(a,b) &= \left(X^{a_{m-1}} Z^{b_{m-1}}\right)\otimes D\left(a',b'\right) \\
           &= (XZ) \otimes D\left(a',b'\right)\\
           &= \left[
\begin{array}{c|c}
    &   -D(a',b')    \\
\hline
D(a',b') &
\end{array}
\right] 
\end{align*}
Consider the two cases.
\begin{itemize}
    \item If $a'b'^T= 1$, then $D(a',b')$ is antisymmetric by the induction hypothesis. But observe that $D(a,b)$ is symmetric in this case, and indeed $ab^T =0$.
    \item If $a'b'^T=0$, then $D(a',b')$ is symmetric by the induction hypothesis. Clearly, $D(a,b)$ is antisymmetric, and in this case, $ab^T = 1$.
\end{itemize}
Finally, 
\begin{align*}
    E(a,b)^2 &= i^{ab^T+ab^T} D(a,b)D(a,b)^T\\
             &= I_N.
\end{align*}
\textbf{(b).} First we notice that
\[
    E(a,b)E(c,d) = i^{ab^T +cd^T} (-1)^{bc^T}D(a+c,b+d).
\]
Similarly,
\[
    E(c,d)E(a,b) = i^{ab^T +cd^T} (-1)^{ad^T}D(a+c,b+d).
\]
Also,
\[
    E(a+c,b+d) = i^{(a+c)(b+d)^T} D(a+c,b+d) = i^{ab^T+ad^T +cb^T +cd^T}D(a+c,b+d).
\]
Now if $E(a,b)E(c,d)=E(c,d)E(a,b)$, then $bc^T=ad^T$. Therefore, $$E(a+c,b+d) = i^{ab^T+cd^T}D(a+c,b+d).$$ It follows that $E(a,b)E(c,d) = \pm E(a+c,b+d)$.

On the other hand, if $E(a,b)E(c,d)\neq E(c,d)E(a,b)$, then $bc^T +ad^T =1$. Hence, 
\[
E(a+c,b+d) = i \cdot i^{ab^T+cd^T}D(a+c,b+d).
\]
In this case, we have $E(a,b)E(c,d) = \pm i E(a+c,b+d)$.

\question{7}{Hermitian matrix}
\textbf{(a).} No.

\textbf{(b).} Let $\langle A,B \rangle = Tr\left(\overline{A^T},B\right)$ denote the inner product of two matrices. Since $E(a,b)^2 = I_N$, $\left\langle \frac{1}{\sqrt{N}} E(a,b),\frac{1}{\sqrt{N}}E(a,b) \right\rangle = 1$. Also, there are $N^2$ many matrices of the form $\frac{1}{\sqrt{N}} E(a,b)$, since there are $N$ different $a$ and $N$ different $b$. 

Now it is left to show that $\langle E(a,b), E(c,d) \rangle = 0$ when $E(a,b)\neq E(c,d)$. Since $E(a,b)$ is Hermitian and $E(a,b)E(c,d) = \pm E(a+c,b+d)$ or $\pm iE(a+c,b+d)$, it suffices to show that $Tr(E(x,y))= 0$. Consider $D(x,y)= D(x,0)D(0,y)$. Here, $D(x,0)$ is symmetric, since it is a tensor product of symmetric matrices, and $D(0,y)$ is antisymmetric and in fact diagonal, since is is a tensor product of diagonal matrices. Therefore, $Tr(D(x,y))=Tr(E(x,y))=0$.

\question{8}{Eigenspace of $E(a,b)$}
\textbf{(a).} Let $Mu = \lambda u$. Then $M^2 u = \lambda Mu$. Since $M^2=I$, $Mu= \frac{1}{\lambda}u$. Hence, $\lambda = 1/\lambda$, and $\lambda = \pm 1$.

\textbf{(b).} Since $E(a,b)$ is Hermitian, there exists a set of $N$ orthogonal  eigenvectors. Therefore, it suffices to show that $+1$ and $-1$ have equal algebraic multiplicity. Let $A=E(a,b)$ and $B=E(a,b+1)$. By question \textbf{6(c)}, $A$ anticommutes with $B$,
\[
AB=-BA.
\]
Hence,  $Au=u$ if and only if $A(Bu)=-(Bu)$. Similarly, $Au=-u$ if and only if $A(Bu)=Bu$. This establishes a bijection between eigenvectors corresponding to $+1$ and to $-1$ eigenvalues. It follows that $+1$ and $-1$ eigenvalues correspond to equal number of eigenvectors.

\textbf{(c).} First consider the operator $\frac{I_N+E(a,b)}{2}$ acting on an eigenvector $u$ of $E(a,b)$ that corresponds to $+1$ eigenvalue.
\begin{align*}
    \frac{I_N+E(a,b)}{2} \cdot u & = \frac{I_N}{2}u + \frac{u}{2}\\
                                 &= u
\end{align*}
Therefore, this operator preserves the eigenspace $V_+$. But if $u'$ corresponds to a $-1$ eigenvalue, then notice that 
\begin{align*}
    \frac{I_N+E(a,b)}{2} \cdot u & = \frac{I_N}{2}u - \frac{u}{2}\\
                                 &= 0.
\end{align*}
Hence, it maps the eigenspace $V_-$ to $\{0\}$. 

On the other hand, $\frac{I_N-E(a,b)}{2}$ preserves $V_-$ and maps $V_+$ to $\{0\}$.

\question{9}{Conjugacy of Heisenberg-Weyl group}
\textbf{(a).} The conjugacy classes for the Heisenberg-Weyl group are 
\begin{enumerate}[label=(\arabic*)]
    \item $\{I_N\}$
    \item $\{-I_N\}$
    \item $\{iI_N\}$
    \item $\{-iI_N\}$
    \item $\{D(1^m,0),-D(1^m,0)\}$
    \item $\{iD(1^m,0),-iD(1^m,0)\}$
    \item $\{D(0,1^m),-D(0,1^m)\}$
    \item $\{iD(0,1^m),-iD(0,1^m)\}$
    \item $\{D(1^m,1^m),-D(1^m,1^m)\}$
    \item $\{iD(1^m,1^m),-iD(1^m,1^m)\}$
\end{enumerate}

\textbf{(b).} The claim is incorrect. Consider the trivial subgroup $H=\{I_N\}$. It is clearly normal but does not contain $-I_N$.

\textbf{(c).} The only centers of $HW_N$ are $Z=\{\pm I_N\}$. Consider the map $f:G\rightarrow Aut(G)$ from $G$ to its automorphism group, where $f(g)= \phi_g$ and $\phi_g(h) =ghg^{-1}$, an automorphism of $G$. Now the kernel of this map is exactly $Z$, and the image is the set $Inn(G)$ of automorphisms that are defined  by conjugation. Each element in $Inn(G)$ is indexed uniquely by an element $(a,b)\in \mathbb{F^{2m}_2}$. Hence, $HW_N / \langle \pm I_N \rangle \cong F_2^{2m}$.
\end{document}

