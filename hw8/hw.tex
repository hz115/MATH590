\documentclass[11pt]{article}
\usepackage{amsmath,amssymb,amsthm,xcolor}
\definecolor{newblue}{rgb}{0.2,0.2,0.6}
\usepackage[colorlinks,pagebackref,allcolors=newblue]{hyperref}
\usepackage{graphicx}
\usepackage[sc]{mathpazo}
\usepackage[hmargin=0.7in,vmargin=1in]{geometry}
\usepackage{fancyhdr,microtype}
\usepackage{algorithm2e}

\pagestyle{fancyplain}
\renewcommand{\headrulewidth}{0pt}

\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt plus 1pt}
\setlength{\headheight}{13.6pt}

\newcommand\question[2]{\vspace{.25in}\hrule\textbf{{\large #1: #2}}\vspace{.5em}\hrule\vspace{.10in}}
\lhead{\textbf{{\large \NAME\ (\ID)}}}
\chead{\textbf{{\large Problem Set \HWNUM}}}
\rhead{{\large MATH 590, Fall 17}}

\DeclareMathOperator{\E}{\mathbb{E}}

\begin{document}
\newcommand\NAME{Fred Zhang}
\newcommand\ID{hz115}
\newcommand\HWNUM{8}
\vspace*{-10mm}
\question{1}{Reed-Solomon Code}
\textbf{(a).} Each codeword must satisfy the parity-checks 
\[
    c(\alpha) = c\left(\alpha^2\right) =\cdots =c\left(\alpha^{d-1}\right) =0.
\]

\textbf{(b).}  In the first case, suppose $\beta =1$. Then 
\[
    \sum_{i=0}^{Q-2} \beta^i =\sum_{i=0}^{Q-2} 1 = Q-1.
\]
Since $Q-1=p^m-1$ and $p^m-1 \equiv p-1\ (\textrm{mod}\ p)$, we have $\sum_{i=0}^{Q-2} \beta^i = p-1$. Now suppose $\beta \neq 1$. Since $1-x^n = (1-x)\left(x^{n-1}+x^{n-2}+\cdots+x+1\right)$,
\[
    \sum_{i=0}^{Q-2} \beta^i = \frac{1-x^{Q-1}}{1-x}.
\]
But notice that $x^{Q-1} = 1$, as the multiplicative group of $F$ has order $Q-1$. Hence, $\sum_{i=0}^{Q-2} \beta^i = 0$.

\textbf{(c).} Let $q(x)$ be in $\mathcal{C}'$. We want to show that $q(x)$ is divisible by $g(x)$. This implies that $q(x) \in \mathcal{C}$. Hence, it suffices to show that $q\left(\alpha^j\right)=0$ for all $1\le j \leq d-1$; if so, every root of $g(x)$ is also a root of $q(x)$, and we have $g(x) | q(x)$.  We expand the expression of $q\left(\alpha^j\right)$.
\begin{align*}
    q\left(\alpha^j\right) &= \sum_{i=0}^{n-1} m\left(\alpha^i\right)\cdot \left(\alpha^j\right)^i \\
                           &= \sum_{i=0}^{n-1}\left(\sum_{l=0}^{k-1} m_l \alpha^{il}\right)\cdot (\alpha^j)^i\\
                           &= \sum_{l=0}^{k-1}m_l \sum_{i=0}^{n-1} \alpha^{i(j+l)}
\end{align*}
Now we note that since $j\leq d-1 = n-k$ and $l \le k-1$, $j+l < n$. Therefore, $\beta =\alpha^{j+l}\neq 1$. By the identity above in \textbf{(b)}, we have 
\[
    \sum_{i=0}^{n-1} \beta^i =0.
\]
It follows that $q\left(\alpha^j\right) = 0$ for $i\leq j \leq d-1$. This shows that $\mathcal{C}' = \mathcal{C}$. 

\textbf{(d).} Let $m(x)$ be a message and $c(x) = \sum_{i=0}^{n-1} c_i x^i$, where $c_i= m(\alpha_i)$. Since $\mathcal{C}$ is linear, it suffices to show the Hamming weight of $c=(c_1,c_2,\cdots, c_{n})$ is at least $d$. By definition, $m(x)$ is a polynomial of degree at most $k-1$. Therefore, it has at most $k-1$ roots. Hence,  $c$ has at most $k-1$ zeros. Since $d= n-k+1$, we have that the Hamming weight of $c$ is at least $d$.

\question{2}{BCH Code}
Let $\alpha$ be a primitive element in $\mathbb{F}_{q^m}^n$. Notice that primitive narrow-sense Reed-Solomon code gives an equivalent definition of RS code, namely,
\[
    RS= \left\{\left(c_0,c_1,\cdot,c_{n-1}\right) \in \mathbb{F}_{q^m}^n\,\middle|\, c(\alpha)= c(\alpha)=\cdots=c\left(\alpha^{n-k}\right) =0 \right\}.
\]
Similarly, BCH code can be equivalently defined as
\[
BCH = \left\{\left(c_0,c_1,\cdot,c_{n-1}\right) \in \mathbb{F}_{q}^n\,\middle|\, c(\alpha)= c(\alpha)=\cdots=c\left(\alpha^{n-k}\right) =0 \right\}.
\]
Observe that a codeword in $BCH$ satisfies the exactly the same set of parity checks for $RS$. Hence, $BCH$ is a subcode of $RS$. In particular, the codeword lies in the base field of $\mathbb{F}_{q^m} \subseteq \mathbb{F}_{q^m}^n$. Therefore, BCH code  is a subfield subcode of Reed-Solomon code.

\question{3}{Decoding Reed-Solomon Code}
\textbf{(a).} We calculate that 
\begin{align*}
    r(2)&= 3, r\left(2^2\right) = 0, r\left(2^3\right)=8\\
    r\left(2^4\right) &= 4, r\left(2^5\right)=5,r\left(2^6\right)=2.
\end{align*}

\textbf{(b).} Let $S_i= r\left(2^i\right)$. Then by definition of the error-locator polynomial,
\[
    A= \begin{bmatrix}
        r(2)& r(4)& r(8)\\
        r(4)& r(8) & r(16)\\
        r(8)&r(16)&r(32)
    \end{bmatrix}
    =\begin{bmatrix}
        3& 0& 8\\
        0& 8 & 4\\
        8&4&5
    \end{bmatrix}.
\]

\textbf{(c).} We solve the following linear system over $\mathbb{F}_{13}$.
\[
\begin{bmatrix}
        3& 0& 8\\
        0& 8 & 4\\
        8&4&5
    \end{bmatrix} \cdot \begin{bmatrix}
        \Lambda_3 \\
        \Lambda_2 \\
        \Lambda_1 
    \end{bmatrix} = - \begin{bmatrix}
        4\\
        5\\
        2
    \end{bmatrix}
\]
This gives $\Lambda_3 = 1, \Lambda_2 = 12, \Lambda_1=4$. Hence, the error-locator is 
\[
    \Lambda(x) = 1+4x+12x^2+x^3.
\]

\textbf{(d).} By calculation, for $i=0,1,\cdots, 11$, we have the following list of values of $\Lambda\left(2^i\right)$.
\[
    \{5, 0, 0, 0, 5, 10, 8, 7, 9, 4, 5, 11\}.
\]
Note that we have zeroes at location $1,2,3$. This implies that the errors occur at location $l_1=9,10,11$.

\textbf{(e).} To calculate the magnitudes of the errors, we write the matrix equation.
\[
\begin{bmatrix}
    2^9 & 2^{10}&2^{11}\\
    4^9 & 4^{10}&4^{11}\\
    8^9 & 8^{10}&8^{11}
\end{bmatrix}\cdot \begin{bmatrix}
    e_1\\e_2\\e_3
\end{bmatrix} = \begin{bmatrix}
    S_1\\S_2\\S_3
\end{bmatrix}=\begin{bmatrix}
    3\\0\\8
\end{bmatrix}.
\]
Solving this over $\mathbb{F}_{13}$ gives $e_1  =e_3=8,e_2=5$.

\question{4}{Decoding BCH Code}
\textbf{(a).} Using the addition table from the handout, we have $S_1=r(\alpha) = \alpha^{13},S_2 = r\left(\alpha^2\right) = \alpha^{11}, S_3=r\left(\alpha^3\right)=1, S_4= r\left(\alpha^4\right)=\alpha^7, S_5=S_6=1$. 

\textbf{(b).} Notice that 
\begin{align*}
    S_1^2 = \alpha^{26} = \alpha^{11} = S_2\\
    S_2^2 = \alpha^{22} = \alpha^{6} = S_4\\
    S_3^2 = 1 = S_6.
\end{align*}

\textbf{(c).} Using the notation from the handout, we run the Extended Euclidean algorithm, starting with $a_1(x)=x^6,u_1(x)=1,v_1(x)=0$. The algorithm proceeds as follows, and we end up with $\Lambda(x) = v_j(x) / \alpha^7$.
\begin{table}[htbp]
\centering
\begin{tabular}{|l|l|l|l|l|}
\hline
$j$                                      & $a_j(x)$                                                          & $u_j(x)$             & $v_j(x)$                                                 & $q_j(x)$             \\ \hline
$2$                                      & $x^5 +x^4 +\alpha^7x^3 +x^2 +\alpha^{11}x+\alpha^13$              & $0$                  & $1$                                                      & $x+1$                \\ \hline
$3$                                      & $\alpha^9x^4 +\alpha^9x^3 +\alpha^{12}x^2 +\alpha^4x+\alpha^{13}$ & $1$                  & $x+1$                                                    & $\alpha^7x$          \\ \hline
$4$                                      & $\alpha^{13}(1+x)+ \alpha^5x^2+\alpha^4x^3 $                      & $\alpha^6x$          & $\alpha^6x(1+x)+1$                                       & $\alpha^5x+\alpha^9$ \\ \hline
$5$ & $\alpha^{11}x^2+\alpha^5$                                         & $1+x+\alpha^{11}x^2$ & $\alpha^{11}x^3 + \alpha^{12}x^2 + \alpha^5x + \alpha^7$ & $\alpha^8x+\alpha^5$ \\ \hline
\end{tabular}
\end{table}

Since $v_5(x) = \alpha^7 \Lambda(x)$, we get 
\[
    \Lambda(x) = 1+\alpha^{13}x+\alpha^5x^2+\alpha^4x^3.
\]
By enumeration, we find that $\Lambda(\alpha^3)=\Lambda(\alpha^{10})=\Lambda(\alpha^{13})=0$. Hence, the errors occur at location $\{12,5,2\}$. The magnitudes of the errors are $1$, so we have the decoded message
\[
    c(x) = r(x) + \left(x^2+x^5+x^{12}\right).
\]
\end{document}
