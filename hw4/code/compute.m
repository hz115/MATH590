%%%%%%%% 1c %%%%%%%%%%
C = {'k','b','r','g','y',[.5 .6 .7]}; % Cell array of colors.
i = 1;

e = .5;
figure
title('Erasure Probabilities for p = .5')
xlabel('Index')
ylabel('Erasure Probabilities')
hold on

for n=4:2:16
    if n == 14
        continue
    end
    p = sort(erasure_prob(e, n));
    x = [1:2^n]/2^n;
    plot(x, p, 'd', 'color',C{i}, 'marker', 'o')
    hold on
    i = i + 1;
end

legend({'n=4';'n=6';'n=8';'n=10';'n=12';'n=16'})
%%%%%%%%%%%%%%%%%%%

%% 1e

e = .5;
figure
title('Fraction of erasure probablities in [.01,.99] for p = .5')
xlabel('Block length n')
ylabel('Fraction')
hold on

f = [];

for n=1:16
    p = erasure_prob(e, n);
    f(n) = sum(p >= .01 & p <= .99) / 2^n;
end

plot(1:16, f, 'd', 'marker', 'o')

%% 1 (f)
n = 10;
e = .2;
error = 1;
p = sort(erasure_prob(e,n));
for i=1:2^10
    error = (1-p(i)) * error;
    if error < .9
        break
    end
end
error
i-1