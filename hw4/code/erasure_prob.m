function [prob] = erasure_prob(p, n)
    for i = 1:n
        p = [2*p - p.^2, p.^2];
    end
    prob = p;