\documentclass[11pt]{article}
\usepackage{amsmath,amssymb,amsthm,xcolor,bbm}
\usepackage[framed,numbered,autolinebreaks,useliterate]{mcode}
\definecolor{newblue}{rgb}{0.2,0.2,0.6}
\usepackage[colorlinks,pagebackref,allcolors=newblue]{hyperref}
\usepackage{graphicx}
\usepackage[sc]{mathpazo}
\usepackage[margin=.9in]{geometry}
\usepackage{fancyhdr}
\usepackage{algorithm2e}

\pagestyle{fancyplain}
\renewcommand{\headrulewidth}{0pt}

\setlength{\parindent}{0pt}
\setlength{\parskip}{5pt plus 1pt}
\setlength{\headheight}{13.6pt}
\newcommand{\que}{\mathord{?}}


\newcommand\question[2]{\vspace{.25in}\hrule\textbf{{\large #1: #2}}\vspace{.5em}\hrule\vspace{.10in}}
\lhead{\textbf{{\large \NAME\ (\ID)}}}
\chead{\textbf{{\large Problem Set \HWNUM}}}
\rhead{{\large MATH 590, Fall 17}}

\DeclareMathOperator{\E}{\mathbb{E}}

\begin{document}
\newcommand\NAME{Fred Zhang}
\newcommand\ID{hz115}
\newcommand\HWNUM{1}
\vspace*{-10mm}
\question{1}{Construction of Binary Polar Codes}
\textbf{(a).}
The estimate $\widehat{U}_1$ is $\que$ unless neither $x_1,x_2$ are erased by the channel. Hence,
\[
    \Pr \left(\widehat{U}_1 =\que\right) = 1- (1-\epsilon)^2 = 2\epsilon - \epsilon^2. 
\]
We can recover $U_2$ unless both $x_1,x_2$ are erased. Therefore,
\[
    \Pr \left(\widehat{U}_2 = \que\right) = \epsilon^2.
\]

\textbf{(b).}
\begin{figure}[htbp]
    \centering
    \includegraphics[scale=0.7]{./img/1b.eps}
    \caption{Factor graph of $N=4$ binary polor code}
    \label{fig:n4-polar}
\end{figure}

The probability that the receiver cannot recover $U_i$ is
\[
    \Pr \left(\widehat{U}_1 =\que\right) = 1- (1-\epsilon)^4
\]
\[
    \Pr \left(\widehat{U}_2 =\que\right) = \left(2\epsilon - \epsilon^2\right)^2 
\]
\[
    \Pr \left(\widehat{U}_3 =\que\right) = 2\epsilon^2 - \epsilon^4 
\]
\[
    \Pr \left(\widehat{U}_3 =\que\right) = 2\left(2\epsilon - \epsilon^2\right) - \left(2\epsilon -\epsilon^2\right)^2
\]

\textbf{(c).} The following MATLAB function returns a vector of probabilities.
\begin{lstlisting}
function [prob] = erasure_prob(p, n)
    for i = 1:n
        p = [2*p - p.^2, p.^2];
    end
    prob = p;
\end{lstlisting}
When $n=1, p=.25$, this gives $(0.4375, 0.0625)$. When $n=2, p=.25$, this gives $(0.6836,    0.1211$,    $0.1914,    0.0039)$. Both match the formula given by \textbf{(b)}.
\newpage
\textbf{(d).}
\begin{figure}[htbp]
    \centering
    \includegraphics[width=\linewidth]{./img/polar.eps}
    \caption{Erasure probabilities for different lengths}
    \label{fig:polar}
\end{figure}

We see that when block length gets larger, the polarization is more obvious. That is, most channels tend to have erasure probabilities either $0$ and $1$. The transition also becomes sharper, and the threshold tends to $C=1-\epsilon =.5$.

\textbf{(e).} See Figure~\ref{fig:frac}.

\begin{figure}[htbp]
    \centering
    \includegraphics[scale=.6]{./img/frac.eps}
    \caption{Fraction of channels with erasure probabilities $[0.01,0.99]$ for different block length}
    \label{fig:frac}
\end{figure}

We observe that as $n$ becomes larger, the fraction of mediocre channels becomes less. The polarization gets more effective.

\textbf{(f).} Use the binary polar code of block length $N$ and encode the information bits on the top $700$ virtual channels with least erasure probabilities. The probability that none of them is a erasure is at least $0.9$, so this achieves our requirement. 

The rate of the code is $700/1024=.684$, which is not too far from the capacity.
\newpage

\question{2}{Scaling Exponent of Binary Polar Code}
\textbf{(a).}  Suppose for a contradiction that $\Pr (X \geq a) > \frac{\E [X]}{a}$. By the definition of expectation,
\[
    \E[X] \geq a \cdot \Pr (X\geq a)  > a\cdot \frac{\E [X]}{a} = \E[X].
\]
This is a contradiction.

\textbf{(b).} Notice that when $x \in [\delta, 1-\delta]$, $\sqrt{x(1-x)}/\sqrt{\delta(1-\delta)}\geq 1 = \mathbbm{1}_{[\delta,1-\delta]}(x)$. When $x\notin  [ \delta, 1-\delta ]$, again, $\sqrt{x(1-x)}/\sqrt{\delta(1-\delta)}\geq 0 = \mathbbm{1}_{[\delta,1-\delta]}(x)$. It follows that 
\[
    \Pr (X\in [\delta, 1-\delta]) \le \E \left[ \frac{\sqrt{X(1-X)}}{\sqrt{\delta(1-\delta)}}\right]
\]

\textbf{(c).} By \textbf{(b)},
\begin{align*}
    \Pr \left[ X_n \in [\delta, 1-\delta] \,\middle|\, X_0 = \epsilon\right] &\le \E \left[ \frac{\sqrt{X_n(1-X_n)}}{\sqrt{\delta(1-\delta)}} \,\middle|\, X_0 =\epsilon \right]\\
                                                                  &= \E \left[ \frac{{g_0(X_n)}}{\sqrt{\delta(1-\delta)}} \,\middle|\, X_0 =\epsilon \right]\\
                                                                  &= \frac{g_n(\epsilon)}{\sqrt{\delta(1-\delta)}}
\end{align*}

\textbf{(e).} We prove the claim by induction. For base case,
\begin{align*}
    g_1(x) &= (Tg_0)(x) && (\text{by definition of } g_1(x) )\\
           &\le \lambda g_0 (x) && (\text{by definition of } \lambda).
\end{align*}
Assume that $g_n(x) \leq \lambda^n g_0(x)$, and consider $g_{n+1}(x)$,
\begin{align*}
    g_{n+1} (x ) &= (Tg_n)(x)&& (\text{by definition of } g_{n+1}(x) )\\
                 &\le \lambda^n (Tg_0) (x) && (\text{by induction hypothesis and linearity of the operator } T )\\
                 &\le \lambda^{n+1} g_0(x).
\end{align*}
This completes the induction step.

\begin{figure}[htbp]
    \centering
    \includegraphics[scale=.43]{./img/lambda.eps}
    \caption{$(Tg_0)(x)/g_0(x)$ for $x\in [0,1]$}
    \label{fig:lambda}
\end{figure}
This is maximized approximately to be $\lambda \approx 0.866$.

\textbf{(f).} 
\begin{align*}
    \Pr\left(X_n \in \left[0.01,0.99\right] \,\middle|\, X_0 = 0.5\right) &\le \frac{g_n(0.5)}{\sqrt{0.01\cdot 0.99}}\\
                                                &\le \lambda^n g_0(0.5) \cdot \frac{1}{\sqrt{0.01\cdot 0.99}} \\
                                                &\approx 5.025 \cdot 0.886^n \\
                                                &= 5.025 \cdot 0.886^{\log_2 N} \\
                                                &= 5.025 \cdot N^{\log_2 0.866}\\
                                                &= 5.025 N^{-0.207}.
\end{align*}

If $g_0(x) = (x(1-x))^{0.66}$, we can calculate numerically via MATLAB that $\lambda\approx 0.832$. Therefore,
\begin{align*}
    \Pr\left(X_n \in \left[0.01,0.99\right] \,\middle|\, X_0 = 0.5\right) &\le \frac{g_n(0.5)}{( 0.01\cdot 0.99 )^{0.66}}\\
                                                                          &= 0.832^n \frac{0.25^{0.66}}{0.0099^{0.66}}\\
                                                                          &\le 9N^{-0265}
\end{align*}

\question{3}{Binary Reed-Muller and Polar Code}
\textbf{(a).} The generating set is $\mathcal{G} = \{v_0,v_1,v_2,v_3 \}$ where
\begin{align*}
    v_0 &= (1,1,1,1,1,1,1,1)\\
    v_1 &= (1,0,1,0,1,0,1,0)\\
    v_2 &= (1,1,0,0,1,1,0,0)\\
    v_3 &= (1,1,1,1,0,0,0,0).
\end{align*}
The generator matrix is 
\[
    G = \begin{pmatrix}
        1&1&1&1&1&1&1&1\\
        1&0&1&0&1&0&1&0\\
        1&1&0&0&1&1&0&0\\
        1&1&1&1&0&0&0&0
    \end{pmatrix}
\]
Since the rows are linearly independent, the code is of dimension $4$.

\textbf{(b).} We show by induction that dimension of $(r,m)$ Reed-Muller code is 
\[
    \binom{m}{0} + \binom{m}{1} + \cdots + \binom{m}{r}.
\]
When $m=1$, our claim holds true as the generator matrix has linearly independent rows. Let $\mathcal{R}(r,m)$ denote the $(r,m)$ binary Reed-Muller code. Suppose for induction that $\mathcal{R}(r,m-1)$ has dimension $\sum_{i=0}^r \binom{m-1}{i}$ for $0\leq i <m$. Any codeword in $\mathcal{R}(r,m)$ can be written as $c_1+c_2$ where $c_1\in \mathcal{R}(r-1,m-1)$ and $c_2\in \mathcal{ R }( r,m-1 )$. Therefore, the dimension of $\mathcal{R}(r,m)$ is the sum of dimensions of $\mathcal{R}(r,m-1)$ and $\mathcal{R}(r-1,m-1)$. Noticing the identity
\[
    \binom{m}{i} = \binom{m-1}{i-1} + \binom{m-1}{i}
\]
completes our induction step, since the RHS are the dimensions of $\mathcal{R}(r-1,m-1)$ and $\mathcal{R}(r,m-1)$, respectively.

\textbf{(c).} The generator matrix $G_N$ is
\[
\begin{pmatrix}
1&0&0&0&0&0&0&0\\
1&0&0&0&1&0&0&0\\
1&0&1&0&0&0&0&0\\
1&0&1&0&1&0&1&0\\
1&1&0&0&0&0&0&0\\
1&1&0&0&1&1&0&0\\
1&1&1&1&0&0&0&0\\
1&1&1&1&1&1&1&1\\
\end{pmatrix}
\]
The rows correspond to the vectors representing the monomials in $\mathcal{M}_3$.

The first row is $f(x_1,x_2,x_3)=x_1x_2x_3$, the second is $x_1x_2$, the third $x_2x_3$, and so on.

\textbf{(d).} The esasure probabilities for the $8$ effective channels are given by
\[
\begin{pmatrix}
0.99609&0.68359&0.80859&0.12109&0.87891&0.19141&0.31641&0.0039062
\end{pmatrix}
\]
To achieve rate $1/2$, we would choose rows that correspond to monomials $x_1,x_2,x_3$ and $1$ (that is, $x_1^0x_2^0x_3^0$).

\textbf{(e).} Both codes choose the same set of vectors as rows in generator matrix. Reed-Muller code tends to choose vectors  of the highest Hamming weights possible.

\end{document}
